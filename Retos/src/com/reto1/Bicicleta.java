package com.reto1;

import java.util.ArrayList;
import java.util.List;

public class Bicicleta {

    private String Id;
    private boolean EnServicio;

    public Bicicleta(String _Id, boolean _EnServicio) {
        Id = _Id;
        EnServicio = _EnServicio;
    }

    public String getId() {
        return Id;
    }

    public boolean getEnServicio() {
        return EnServicio;
    }
}
