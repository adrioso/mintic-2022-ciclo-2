package com.reto1;

import java.util.ArrayList;
import java.util.List;

public class Estacion {

    private int Id;
    private String Ubicacion;
    private Bicicleta[] bicicletas;

    public Estacion(int id, String ubicacion, Bicicleta[] bicicletas) {
        Id = id;
        this.Ubicacion = ubicacion;
        this.bicicletas = bicicletas;
    }

    public Bicicleta[] BicicletasEnServicio(){
        List<Bicicleta> EnServicio= new ArrayList<Bicicleta>();
        for (int i = 0; i< bicicletas.length; i++){
            if (bicicletas[i].getEnServicio()){
                EnServicio.add(bicicletas[i]);
            }
        }
        return (EnServicio).toArray(new Bicicleta[0]);
    }
    public Bicicleta[] BicicletasDisponibles(){
        List<Bicicleta> Disponible= new ArrayList<Bicicleta>();
        for (int i = 0; i< bicicletas.length; i++){
            if (!bicicletas[i].getEnServicio()){
                Disponible.add(bicicletas[i]);
            }
        }
        return (Disponible).toArray(new Bicicleta[0]);
    }
    public boolean EncontrarBicicleta(String Id){
        for (int i = 0; i< bicicletas.length; i++){
            if (bicicletas[i].getId()== Id){
                return true;
            }
        }
        return false;
    }
    public Bicicleta ConsultarBicicleta(String Id){
        for (int i = 0; i< bicicletas.length; i++){
            if (bicicletas[i].getId()== Id){
                return bicicletas[i];
            }
        }
        return null;
    }

    public static boolean RequiereExpansion(Estacion[] estaciones) {
        int Total_BicicletasEstacion=0;
        int Bicletas_EnServicio=0;
        int Bicletas_Disponibles=0;

        for (int i = 0; i < estaciones.length; i++) {
            Total_BicicletasEstacion = estaciones[i].bicicletas.length + Total_BicicletasEstacion;
            Bicletas_EnServicio = estaciones[i].BicicletasEnServicio().length + Bicletas_EnServicio;
            Bicletas_Disponibles = estaciones[i].BicicletasDisponibles().length + Bicletas_Disponibles;
        }
        if (Bicletas_Disponibles<(Total_BicicletasEstacion*0.15)) {
            return true;
        }
        return false;
    }
}
